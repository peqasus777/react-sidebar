import React, { useState, useRef } from "react";
import Navbar from "./components/Navbar";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import "./style/index.css";

function App() {
  const [navVisible, showNavbar] = useState(false);
  const scrollContainerRef = useRef();

  return (
    <BrowserRouter>
      <div className="App">
        <Navbar visible={navVisible} show={showNavbar} />
        <div ref={scrollContainerRef} className="horizontal-scroll-container">
          <Routes>
            <Route path="/" element={<Navigate to="/About Me" />} />
            <Route
              path="/About Me"
              element={
                <div className={!navVisible ? "page" : "page page-with-navbar"}>
                  <h1>About Me</h1>
                </div>
              }
            />
            <Route
              path="/Education"
              element={
                <div className={!navVisible ? "page" : "page page-with-navbar"}>
                  <h1>Education</h1>
                </div>
              }
            />
            <Route
              path="/Experience"
              element={
                <div className={!navVisible ? "page" : "page page-with-navbar"}>
                  <h1>Experience</h1>
                </div>
              }
            />
            <Route
              path="/Portfolio"
              element={
                <div className={!navVisible ? "page" : "page page-with-navbar"}>
                  <h1>Portfolio</h1>
                </div>
              }
            />
            <Route
              path="/Contacts"
              element={
                <div className={!navVisible ? "page" : "page page-with-navbar"}>
                  <h1>Contacts</h1>
                </div>
              }
            />
            <Route
              path="/Feedback"
              element={
                <div className={!navVisible ? "page" : "page page-with-navbar"}>
                  <h1>Feedback</h1>
                </div>
              }
            />
          </Routes>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
