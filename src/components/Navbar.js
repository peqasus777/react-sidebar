import React from 'react';
import {
	FaAngleRight,
	FaAngleLeft, 
	FaChartBar, 
	FaThLarge, 
	FaShoppingCart, 
	FaCog,
	FaSignOutAlt,
	FaBars
} from 'react-icons/fa';
import { NavLink } from "react-router-dom";
import "../style/navbar.css";

const ICON_SIZE = 20;

function Navbar({visible, show}) {

	return (
		<>
			<div className="mobile-nav">
				<button
					className="mobile-nav-btn"
					onClick={() => show(!visible)}
				>
					<FaBars size={24}  />
				</button>
			</div>
			<nav className={!visible ? 'navbar' : ''}>
				<button
					type="button"
					className="nav-btn"
					onClick={() => show(!visible)}
				>
					{ !visible
						? <FaAngleRight size={30} /> : <FaAngleLeft size={30} />}
				</button>
				<div>
					<NavLink
						className="logo"
						to="/"
					>
							<img
								src={require("../assets/Images/logo.png")}
								alt="logo"
							/>
					</NavLink>
					<div className="links nav-top">
						<NavLink to="/about me" className="nav-link">
							<FaThLarge size={ICON_SIZE} />
							<span>About Me</span>
						</NavLink>
						<NavLink to="/Education" className="nav-link">
							<FaChartBar size={ICON_SIZE} />
							<span>Education </span>
						</NavLink>
						<NavLink to="/Experience" className="nav-link">
							<FaShoppingCart size={ICON_SIZE} />
							<span>Experience</span> 
						</NavLink>
						<NavLink to="/Portfolio" className="nav-link">
							<FaShoppingCart size={ICON_SIZE} />
							<span>Portfolio
								</span> 
						</NavLink>
						<NavLink to="/Contacts" className="nav-link">
							<FaShoppingCart size={ICON_SIZE} />
							<span>Contacts</span> 
						</NavLink>
						<NavLink to="/Feedback" className="nav-link">
							<FaShoppingCart size={ICON_SIZE} />
							<span>Feedback
								</span> 
						</NavLink>
				
					</div>
				</div>

				<div className="links">
					<NavLink to="/Go Back" className="nav-link">
						<FaCog size={ICON_SIZE} />
						<span>Go Back</span> 
					</NavLink>
					<NavLink to="/Sign-out" className="nav-link">
						<FaSignOutAlt size={ICON_SIZE} />
						<span>Logout</span>  
					 </NavLink>
				</div>
			</nav>
		</>
  );
}

export default Navbar;
